function alert() {
    return `
    <div class="alert alert-warning" role="alert">
    Uh oh! Something went wrong! :(
    </div>

    `
}

function createCard(name, description, pictureUrl, starts, ends, locationName) {
    return `
        <div class="card mb-3 shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-center text-muted ">
                <p class="card-text">${starts} - ${ends}</p>
            </div>
        </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        
        let index = 0
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const locationName = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, starts, ends, locationName);
            const column = document.querySelector(`#col-${index % 3}`);
            column.innerHTML += html;
            index += 1;
          }
        }
  
      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
        const errorHTML = alert();
        const somethingWrong = document.querySelector('.something-wrong');
        somethingWrong.innerHTML = errorHTML;
    }
  
  });
